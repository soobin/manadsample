//
//  ManInterstitialAdViewController.h
//  ManAdSample
//
//  Created by MezzoMedia on 2018. 01.
//  Copyright © 2018년 MezzoMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ManInterstitial.h"

@interface ManInterstitialAdViewController : UIViewController <ManInterstitialDelegate> {
//    ManInterstitial *manInterstitialAdView;

    NSString* publisher;
    NSString* media;
    NSString* section;
    NSString* viewType;
}
-(void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s;
-(void)setViewStyle:(NSString*)type;
@end
