//
//  ManBannerAdViewController.h
//  ManAdView
//
//  Created by MezzoMedia on 2018. 01.
//  Copyright © 2018년 MezzoMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADBanner.h"

@interface ManBannerAdViewController : UIViewController <ADBannerDelegate> {
    
    ADBanner *manAdView;
    NSString* publisher;
    NSString* media;
    NSString* section;
}
-(void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s rich:(Boolean)is;
@end
