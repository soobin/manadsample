//
//  ManFlexibleAdInterstitialViewController.m
//  ManAdSample
//
//  Created by MezzoMedia on 2018. 01.
//  Copyright © 2018년 MezzoMedia. All rights reserved.
//

#import "ManFlexibleAdInterstitialViewController.h"

@interface ManFlexibleAdInterstitialViewController ()
@end

@implementation ManFlexibleAdInterstitialViewController


-(void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s{
    publisher= p;
    media= m;
    section= s;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"중간광고 - Flexible";
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // 광고뷰 OR 일반뷰를 가져온다.
    [self getRandomView:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
    [super dealloc];
    
    if (manFlexibleAdInterstitial) {
        [manFlexibleAdInterstitial release];
        manFlexibleAdInterstitial = nil;
    }
    
    if (normalView) {
        //[normalView release];
        //normalView = nil;
    }
}


#pragma mark functions

-(void)getRandomView:(BOOL)bSwipeLeft {
    
    CGFloat startX = 0;
    CGFloat willMoveX = 0;
    
    if (bSwipeLeft) {
        startX = self.view.bounds.size.width;
    } else {
        startX = -1 * self.view.bounds.size.width;
    }
    willMoveX = -1 * startX;
    
    NSInteger randomNum = (NSInteger)random() % 2;
    if (randomNum) {

        manFlexibleAdInterstitial = [[ManBreakAdInterstitial alloc] init];
        manFlexibleAdInterstitial.delegate = self;
        
        
        manFlexibleAdInterstitial.bShowEndAnimation = YES;     // 스와이프기능 사용여부
        manFlexibleAdInterstitial.bShowButtons = YES;          // 좌우 스크롤 버튼 사용여부
        manFlexibleAdInterstitial.bShowCloseButtons = NO;     // 닫기버튼 사용여부

        [manFlexibleAdInterstitial publisherID:publisher mediaID:media sectionID:section];
        
        [self.view addSubview:manFlexibleAdInterstitial];
        
        manFlexibleAdInterstitial.frame = CGRectMake(startX,
                                                  self.view.bounds.origin.y,
                                                  self.view.bounds.size.width,
                                                  self.view.bounds.size.height - 40.0f);
        // 선택 설정
        manFlexibleAdInterstitial.gender = @"2";
        manFlexibleAdInterstitial.age = @"15";
        manFlexibleAdInterstitial.userId = @"skill007";
        manFlexibleAdInterstitial.userEmail = @"skill007@mezzomedia.co.kr";
        manFlexibleAdInterstitial.userPositionAgree = @"1";
        
        // 랜딩페이지 사파리 띄움 여부 설정
        [manFlexibleAdInterstitial useGotoSafari:YES];
        
        // 광고 시작
        [manFlexibleAdInterstitial startBreakAd];
        
        [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
            manFlexibleAdInterstitial.transform = CGAffineTransformTranslate(manFlexibleAdInterstitial.transform, willMoveX, 0);
        }  completion:^(BOOL finished) {
            // 샘플뷰 제거
            [normalView removeFromSuperview];
        }];

    } else {
        
        if (!normalView) {
            normalView = [[UIView alloc] init];
        }
        [self.view addSubview:normalView];
        
        normalView.frame = CGRectMake(startX,
                                      self.view.bounds.origin.y,
                                      self.view.bounds.size.width,
                                      self.view.bounds.size.height);
        normalView.backgroundColor =  [UIColor colorWithRed:(CGFloat)random() / 0x7fffffff
                                                      green:(CGFloat)random() / 0x7fffffff
                                                       blue:(CGFloat)random() / 0x7fffffff
                                                      alpha:1.0f];
        
        [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
            normalView.transform = CGAffineTransformTranslate(normalView.transform, willMoveX, 0);
        }  completion:^(BOOL finished) {
            // 중간광고 제거
            [manFlexibleAdInterstitial removeFromSuperview];
        }];
    }
    
}


- (void)swipeLeft {
    
    [self getRandomView:YES];
}

- (void)swipeRight {
    
    [self getRandomView:NO];
}


#pragma mark touched

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint pt;
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] == 1) {
        UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
        if ([touch tapCount] == 1) {
            pt = [touch locationInView:self.view];
            touchBeganX = pt.x;
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint pt;
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] == 1) {
        UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
        if ([touch tapCount] == 1) {
            pt = [touch locationInView:self.view];
            touchMovedX = pt.x;
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] == 1) {
        int diffX = touchMovedX - touchBeganX;
        if (diffX > 50) {
            [self swipeRight];
        } else if (diffX < -50) {
            [self swipeLeft];
        }
    }
}


#pragma mark ManBreakAdViewDelegate

- (void)didSwipeBreakAd:(BOOL)bLeftSwipe {
    if (bLeftSwipe) {
        NSLog(@"[ManFlexibleAdInterstitialViewController] ============= didSwipeBreakAd - YES");
    }else{
        NSLog(@"[ManFlexibleAdInterstitialViewController] ============= didSwipeBreakAd - NO");
    }
}

- (void)didCloseBreakAd {
    NSLog(@"[ManFlexibleAdInterstitialViewController] ============= didCloseBreakAd");
    [self getRandomView:YES];
}

- (void)didClickBreakAd {
    NSLog(@"[ManFlexibleAdInterstitialViewController] ============= didClickBreakAd");
//    [self getRandomView:YES];
}

- (void)didFailBreakAd {
    NSLog(@"[ManFlexibleAdInterstitialViewController] ============= didFailBreakAd");
    [self getRandomView:NO];
}

- (void)didWebLoadBreakAd {
    NSLog(@"[ManFlexibleAdInterstitialViewController] ============= didWebLoadBreakAd");
}

- (void)didFailWebLoadBreakAd {
    NSLog(@"[ManFlexibleAdInterstitialViewController] ============= didFailWebLoadBreakAd");
    //[self getRandomView:NO];
}

- (void)didCloseRandingPage:(ManBreakAdInterstitial *)manAd {
    NSLog(@"[ManBreakAdInterstitialViewController] ============= didCloseRandingPage");
}


@end
