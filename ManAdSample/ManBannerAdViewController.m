//
//  ManBannerAdViewController.m
//  ManAdView
//
//  Created by MezzoMedia on 2018. 01.
//  Copyright © 2018년 MezzoMedia. All rights reserved.
//

#import "ManBannerAdViewController.h"
#import "UIView+Toast.h"
@implementation ManBannerAdViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

bool isRich;
-(void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s rich:(Boolean)is{
    publisher= [p retain];
    media= [m retain];
    section= [s retain];
    isRich = is;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"배너 광고";
    self.view.backgroundColor = [UIColor orangeColor];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    CGRect screenRect = [[UIScreen mainScreen] nativeBounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    NSLog(@"ManBannerAdViewController ========= Device Size : %f / %f", screenWidth, screenHeight);
    NSLog(@"ManBannerAdViewController ========= Frame Size : %f / %f", self.view.bounds.size.width, self.view.bounds.size.height);
    
    // 배너 리사이징 처리
    CGFloat bannerLimitHeight = 50.0f;                                                                      // 최대 세로 리사이징 제한사이즈
    CGFloat bannerFrameWidth = MIN(self.view.bounds.size.width, self.view.bounds.size.height);              // 배너 가로 프레임 사이즈(디바이스의 portrait 가로사이즈 기준)
    CGFloat bannerFrameHeight = bannerFrameWidth / MAN_BANNER_RATIO;                                        // 배너 세로 프레임 사이즈
    bannerFrameHeight = (bannerFrameHeight > bannerLimitHeight)? bannerLimitHeight : bannerFrameHeight;     // 배너 세로 프레임 사이즈 보정 (제한 사이즈를 넘지 않도록 보정)
    
    // 배너광고 초기화
    manAdView = [[ADBanner alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, bannerFrameHeight)];              // 이미지형 띠배너 사용시 초기화
    //    manAdView = [[ADBanner alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, MAN_BANNER_AD_HEIGHT)];         // 리치미디어형 띠매너 사용시 초기화
    
    //    [manAdView applicationID:@"mezzo/solution" adWindowID:@"banner"];
    //    [manAdView publisherID:@"100" mediaID:@"200" sectionID:@"300"];
    [manAdView publisherID:publisher mediaID:media sectionID:section];
    manAdView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [manAdView useGotoSafari:YES];
    [manAdView useReachMedia:isRich];
    
    manAdView.gender = @"2";
    manAdView.age = @"15";
    manAdView.userId = @"skill007";
    manAdView.userEmail = @"skill007@mezzomedia.co.kr";
    manAdView.userPositionAgree = @"1";

    manAdView.delegate = self;
    [manAdView startBannerAd];

    [self.view addSubview:manAdView];
    
    // 전면광고 버튼
    CGFloat buttonWidth = 200.0f;
    CGFloat buttonHeight = 35.0f;
    
    UIButton *reloadAdButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [reloadAdButton setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width - buttonWidth) / 2,
                                        manAdView.frame.size.height + 50.0f,
                                        buttonWidth,
                                        buttonHeight)];
    
    [reloadAdButton setTitle:@"배너광고 리로드" forState:UIControlStateNormal];
    reloadAdButton.backgroundColor = [UIColor grayColor];
    reloadAdButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    [reloadAdButton addTarget:self action:@selector(reloadAdButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:reloadAdButton];
}


- (CGFloat)getBannerFrameWidth {
    return 320.0f;
}

- (CGFloat)getBannerFrameHeight {
    return 50.0f;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
    NSLog(@"ManBannerAdViewController =================== dealloc!!");
    if (manAdView) {
        [manAdView release];
    }
    
    [super dealloc];
}

- (void)viewWillAppear:(BOOL)animated {
    
    //    [manAdView viewShowState:YES];
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    //    [manAdView viewShowState:NO];
    [super viewDidDisappear:animated];
}

- (BOOL)prefersStatusBarHidden {
    
    // ios7
    return [UIApplication sharedApplication].statusBarHidden;
    
}

#pragma mark buttons

- (void)reloadAdButton:(id)sender {
    [manAdView startBannerAd];
    //    [manAdView stopBannerAd];
}

#pragma mark ADBannerDelegate

- (void)adBannerClick:(ADBanner*)adBanner {
    // 배너 광고 클릭
    NSLog(@"[ManBannerAdViewController] =============== adBannerClick");
}

- (void)adBannerParsingEnd:(ADBanner*)adBanner {
    // 배너광고 파싱 완료
    NSLog(@"[ManBannerAdViewController] =============== adBannerParsingEnd");
}

- (void)didReceiveAd:(ADBanner*)adBanner chargedAdType:(BOOL)bChargedAdType {
    // 배너 광고의 수신 성공 및 유료/무료
    NSString *chargedAdType = nil;
    if (bChargedAdType) {
        chargedAdType = @"유료";
    } else {
        chargedAdType = @"무료";
    }
    [self.view makeToast:chargedAdType];
    NSLog(@"[ManBannerAdViewController] =========== didReceiveAd : %@", chargedAdType);
}

- (void)didFailReceiveAd:(ADBanner*)adBanner errorType:(NSInteger)errorType {
    // 배너 광고 수신 실패
    // errorTyped은 ManAdDefine.h 참조
    NSLog(@"[ManBannerAdViewController] =========== didFailReceiveAd : %ld", (long)errorType);
    NSString *log = @"none";
    switch (errorType) {
        case NewManAdSuccess:
            log = @"성공";
            break;

        case NewManAdRequestError:
            log = @"잘못된 광고 요청";
            break;

        case NewManAdParameterError:
            log = @"잘못된 파라메터 전달";
            break;

        case NewManAdIDError:
            log = @"광고 미존재";
            break;

        case NewManAdNotError:
            log = @"광고 없음 (No Ads)";
            break;

        case NewManAdServerError:
            log = @"광고서버 에러";
            break;

        case NewManAdNetworkError:
            log = @"네트워크 에러";
            break;

        case NewManAdCreativeError:
            log = @"광고물 요청 실패";
            break;
    }

    log = [log stringByAppendingString:[NSString stringWithFormat:@" : %ld", (long)errorType]];

    [self.view makeToast:log];
    //    [manAdView startBannerAd];
}

- (void)didCloseRandingPage:(ADBanner*)adBanner {
    // 배너광고 클릭시 나타났던 랜딩 페이지 닫힐 경우
    NSLog(@"[ManBannerAdViewController] =========== didCloseRandingPage");
}

// 지정된 주기 이내에 광고리로드가 발생됨
- (void)didBlockReloadAd:(ADBanner*)adBanner {
    NSLog(@"[ManBannerAdViewController] =========== didBlockReloadAd");
}

@end
