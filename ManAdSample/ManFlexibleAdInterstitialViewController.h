//
//  ManFlexibleAdInterstitialViewController.h
//  ManAdSample
//
//  Created by MezzoMedia on 2018. 01.
//  Copyright © 2018년 MezzoMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ManAdDefine.h"
#import "ManBreakAdInterstitial.h"

@interface ManFlexibleAdInterstitialViewController : UIViewController <ManBreakAdInterstitialDelegate> {
    UIScrollView *scrollView;
    
    ManBreakAdInterstitial *manFlexibleAdInterstitial;
    UIView *normalView;
    
    CGFloat touchBeganX;
    CGFloat touchMovedX;
    
    NSString* publisher;
    NSString* media;
    NSString* section;
}
-(void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s;
@end
