    //
    //  ManInterstitialAdViewController.m
    //  ManAdSample
    //
    //  Created by MezzoMedia on 2018. 01.
    //  Copyright © 2018년 MezzoMedia. All rights reserved.
    //

#import "ManInterstitialAdViewController.h"
#import "UIView+Toast.h"
@implementation ManInterstitialAdViewController

ManInterstitial *manInterstitialAdView;

-(void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s{
    publisher= [p retain];
    media= [m retain];
    section= [s retain];
}

-(void)setViewStyle:(NSString*)type{
    viewType = type;
}

- (void)viewDidLoad {
    [super viewDidLoad];
        // Do any additional setup after loading the view.

    self.view.backgroundColor = [UIColor orangeColor];
    self.title = @"전면 광고";

    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

        // 전면광고 샘플버튼
    CGFloat buttonWidth = 200.0f;
    CGFloat buttonHeight = 35.0f;

    UIButton *manInterstitialAdButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [manInterstitialAdButton setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width - buttonWidth) / 2,
                                                 100 + 50.0f,
                                                 buttonWidth,
                                                 buttonHeight)];

    [manInterstitialAdButton setTitle:@"전면광고 열기" forState:UIControlStateNormal];
    manInterstitialAdButton.backgroundColor = [UIColor grayColor];
    manInterstitialAdButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    [manInterstitialAdButton addTarget:self action:@selector(manInterstitialAdButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:manInterstitialAdButton];

        // 전면광고 띄움 샘플 함수 호출
    [self openManInterstitial];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    if (manInterstitialAdView) {
        NSLog(@"ManAdSampleController ============ Interstitial Dealloc!");
        [manInterstitialAdView release];
    }
    [super dealloc];
}

    // 전면광고 띄움 함수
- (void)openManInterstitial {
    manInterstitialAdView = [ManInterstitial shareInstance];

    if (manInterstitialAdView) {
        manInterstitialAdView.delegate = self;
        [manInterstitialAdView publisherID:publisher mediaID:media sectionID:section];

            // 선택 세팅
        manInterstitialAdView.gender = @"2";
        manInterstitialAdView.age = @"15";
        manInterstitialAdView.userId = @"skill007";
        manInterstitialAdView.userEmail = @"skill007@mezzomedia.co.kr";
        manInterstitialAdView.userPositionAgree = @"1";
        manInterstitialAdView.viewType = viewType;
        [manInterstitialAdView startInterstitial];
    }
}

- (void)manInterstitialAdButton:(id)sender {
    NSLog(@"[ManInterstitialAdViewController]=========== init Interstitial!");
    [self openManInterstitial];
}


#pragma mark ManInterstitialDelegate

- (void)didReceiveInterstitial {
        // 전면 광고 성공
    NSLog(@"[ManInterstitialAdViewController]=========== didReceiveInterstitial!");
}

- (void)didFailReceiveInterstitial:(NSInteger)errorType {
    NSLog(@"[ManInterstitialAdViewController]=========== didFailReceiveInterstitial : errorType : %ld", (long)errorType);
    NSString *log = @"none";
    switch (errorType) {
        case NewManAdSuccess:
            log = @"성공";
            break;

        case NewManAdRequestError:
            log = @"잘못된 광고 요청";
            break;

        case NewManAdParameterError:
            log = @"잘못된 파라메터 전달";
            break;

        case NewManAdIDError:
            log = @"광고 미존재";
            break;

        case NewManAdNotError:
            log = @"광고 없음 (No Ads)";
            break;

        case NewManAdServerError:
            log = @"광고서버 에러";
            break;

        case NewManAdNetworkError:
            log = @"네트워크 에러";
            break;

        case NewManAdCreativeError:
            log = @"광고물 요청 실패";
            break;

        case NewManAdFileError:
            log = @"광고물 파일 형식 에러";
            break;


    }

    log = [log stringByAppendingString:[NSString stringWithFormat:@" : %ld", (long)errorType]];

        // 전면 광고 수신 에러
    [self.view makeToast:log];

}

- (void)didCloseInterstitial {
        // 전면 광고 닫기
    NSLog(@"[ManInterstitialAdViewController]=========== Close Interstitial!");

    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
}

@end
