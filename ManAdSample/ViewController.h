//
//  ViewController.h
//  ManAdSample
//
//  Created by MezzoMedia on 2018. 01.
//  Copyright © 2018년 MezzoMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIScrollViewDelegate> {

}
@property (retain, nonatomic) IBOutlet UIScrollView *scrollview;

@end
