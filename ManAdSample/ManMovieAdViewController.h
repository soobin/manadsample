//
//  ManMovieAdViewController.h
//  ManAdView
//
//  Created by MezzoMedia on 2018. 01.
//  Copyright © 2018년 MezzoMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ManMovieAdView.h"

@interface ManMovieAdViewController : UIViewController <ManMovieAdViewDelegate> {
    
    MPMoviePlayerController *clientPlayer;
    
    ManMovieAdView *manMovieAdView;
    MOVIE_SCREEN_MODE curScreenMode;
    NSString* publisher;
    NSString* media;
    NSString* section;
    NSString* categoryCD;
}

- (void)startClientMovie;
- (void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s categoryCD:(NSString*)cate;
@end
