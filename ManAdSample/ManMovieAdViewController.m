    //
    //  ManMovieAdViewController.m
    //  ManAdView
    //
    //  Created by MezzoMedia on 2018. 01.
    //  Copyright © 2018년 MezzoMedia. All rights reserved.
    //

#import "ManMovieAdViewController.h"
#import "UIView+Toast.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

    // 동영상 광고 높이
#define MAN_MOVIE_HEIGHT        180
#define MAN_MOVIE_X             0
#define MAN_MOVIE_Y             60

@implementation ManMovieAdViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

    if (self) {
            // Custom initialization
    }

    return self;
}


-(void)setPID:(NSString*)p MID:(NSString*)m SID:(NSString*)s categoryCD:(NSString*)cate{
    publisher= [p retain];
    media= [m retain];
    section= [s retain];
    categoryCD = [cate retain];
}

- (void)viewDidLoad{
    
    [super viewDidLoad];
        // Do any additional setup after loading the view.

    self.view.backgroundColor = [UIColor blackColor];
    self.title = @"동영상 광고(skip)";

    curScreenMode = MODE_NORMAL;

    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(didEnterBackground:)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];

    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(didBecomeActive:)
                                                 name: UIApplicationDidBecomeActiveNotification
                                               object: nil];
    [self muteSoundActivite:YES];
}

- (void)viewDidAppear:(BOOL)animated {

    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;

    if (width > height) {
        [self.navigationController setNavigationBarHidden:YES];
        manMovieAdView = [[ManMovieAdView alloc] initWithFrame:CGRectMake(MAN_MOVIE_X, 0, width, height)];
    }else{
        manMovieAdView = [[ManMovieAdView alloc] initWithFrame:CGRectMake(MAN_MOVIE_X, MAN_MOVIE_Y, width, MAN_MOVIE_HEIGHT)];
    }



        //[manMovieAdView setScreenMode:curScreenMode];
    manMovieAdView.delegate = self;
    [manMovieAdView publisherID:publisher mediaID:media sectionID:section];           // Skip
                                                                                      // 카테고리 설정
    manMovieAdView.categoryCD = categoryCD;
        // 추가정보 설정
    manMovieAdView.gender = @"2";
    manMovieAdView.age = @"15";
    manMovieAdView.userId = @"skill007";
    manMovieAdView.userEmail = @"skill007@mezzomedia.co.kr";
    manMovieAdView.userPositionAgree = @"1";

        //Afreeca UI (음소거 버튼)
        //manMovieAdView.uiType = @"B";

        // 랜딩페이지 사파리 이동 설정
    [manMovieAdView useGotoSafari:YES];

    [self.view addSubview:manMovieAdView];
    [manMovieAdView startMovieAd];

    [super viewDidAppear:animated];
}

-(void)muteSoundActivite:(BOOL)check{
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:check error:nil];
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {

    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    CGFloat temp = 0;
    CGFloat y = 0.0f;

    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        [self.navigationController setNavigationBarHidden:NO];
        if (width > height) {
            temp = width;
            width = height;
            height = MAN_MOVIE_HEIGHT;
            y = MAN_MOVIE_Y;
        }

    } else {

        [self.navigationController setNavigationBarHidden:YES];
        if (height > width) {
            temp = width;
            width = height;
            height = temp;
        }
    }

    if (manMovieAdView) {
        manMovieAdView.frame = CGRectMake(MAN_MOVIE_X, y, width, height);
    }
    if(clientPlayer){
        [[clientPlayer view] setFrame:CGRectMake(MAN_MOVIE_X, y, width, height)];
    }
}

- (void)viewWillLayoutSubviews {
        //    NSLog(@"[ManMovieAdViewController] ============ viewWillLayoutSubviews");
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return (toInterfaceOrientation == UIInterfaceOrientationIsPortrait(toInterfaceOrientation));
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {

    return NO;
}


- (void)dealloc {
    NSLog(@"[ManMovieAdViewController] ============ dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self];

        // S-Plus 광고 제거
    [self removeManAdView];

    [clientPlayer stop];
    [clientPlayer release];
    [super dealloc];
}

    // S-Plus 광고제거
- (void)removeManAdView {
    if(manMovieAdView){
        [manMovieAdView removeFromSuperview];
        manMovieAdView.delegate = nil;
        [manMovieAdView release];
        manMovieAdView = nil;
    }
}

- (void)startClientMovie {
        // S-Plus 광고제거
    [self removeManAdView];

    NSLog(@"[ManMovieAdViewController] ============ startClientMovie");

    if (clientPlayer == nil) {
        NSString *movieUrl = @"http://vod.midas-i.com/20140708174154_middle.mp4";
        if (movieUrl && [movieUrl length] > 0) {
            CGFloat width = [UIScreen mainScreen].bounds.size.width;
            CGFloat height = [UIScreen mainScreen].bounds.size.height;
            CGFloat temp = 0;
            CGFloat y = 0.0f;
            clientPlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:movieUrl]];
            [clientPlayer setMovieSourceType:MPMovieSourceTypeUnknown];

            if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation)){
                if (width > height) {
                    temp = width;
                    width = height;
                    height = MAN_MOVIE_HEIGHT;
                    y = MAN_MOVIE_Y;
                }  // code for Portrait orientation
            }

            [[clientPlayer view] setFrame:CGRectMake(MAN_MOVIE_X, y, width, height)];
            clientPlayer.scalingMode = MPMovieScalingModeAspectFit;
            clientPlayer.controlStyle = MPMovieControlStyleNone;
            [clientPlayer prepareToPlay];
            [clientPlayer play];
            [self.view addSubview:clientPlayer.view];
        }
    }
}

#pragma mark notification

- (void)didEnterBackground:(NSNotification*)notification {
    NSLog(@"[ManMovieAdViewController] ================== didEnterBackground");

}

- (void)didBecomeActive:(NSNotification*)notification {
    NSLog(@"[ManMovieAdViewController] ================== didBecomeActive");

    if (clientPlayer) {
        [clientPlayer play];
    }
}

#pragma mark buttons

- (void)changeScreenButton:(id)sender {

    if (curScreenMode == MODE_NORMAL) {
        curScreenMode = MODE_WIDE;
    } else if (curScreenMode == MODE_WIDE) {
        curScreenMode = MODE_STRETCH;
    } else if (curScreenMode == MODE_STRETCH) {
        curScreenMode = MODE_ORIGNAL;
    } else if (curScreenMode == MODE_ORIGNAL) {
        curScreenMode = MODE_WIDE;
    }
    [manMovieAdView setScreenMode:curScreenMode];
}

#pragma mark ManMovieAdDelegate

/* 동영상 광고 정보 수신 성공
 */
- (void)didReceiveMovieAd:(ManMovieAdView*)manMovieAd {

    NSString *log = @"성공";
    [self.view makeToast:log];

    NSLog(@"[ManMovieAdViewController] ================== didReceiveMovieAd");
}

/* 동영상 광고 수신 에러
 */
- (void)didFailReceiveMovieAd:(ManMovieAdView*)manMovieAd errorType:(NSInteger)errorType {
    NSLog(@"[ManMovieAdViewController] ================== didFailReceiveMovieAd - error : %ld", (long)errorType);
    NSString *log = @"none";
    switch (errorType) {
        case NewManAdSuccess:
            log = @"성공";
            break;

        case NewManAdRequestError:
            log = @"잘못된 광고 요청";
            break;

        case NewManAdParameterError:
            log = @"잘못된 파라메터 전달";
            break;

        case NewManAdIDError:
            log = @"광고 미존재";
            break;

        case NewManAdNotError:
            log = @"광고 없음 (No Ads)";
            break;

        case NewManAdServerError:
            log = @"광고서버 에러";
            break;

        case NewManAdNetworkError:
            log = @"네트워크 에러";
            break;

        case NewManAdCreativeError:
            log = @"광고물 요청 실패";
            break;
    }

    log = [log stringByAppendingString:[NSString stringWithFormat:@" : %ld", errorType]];


    [self startClientMovie];
    [self.view makeToast:log];
}

/* 동영상 광고 종료
 */
- (void)didFinishMovieAd:(ManMovieAdView*)manMovieAd {
    NSLog(@"[ManMovieAdViewController] ================== didFinishMovieAd");
    [self startClientMovie];
}

/* 동영상 광고 플래이 종료 (동영상 광고 플래이 도중 종료)
 */
- (void)didStopMovieAd:(ManMovieAdView*)manMovieAd {
    NSLog(@"===== ManMovieAdViewController - didStopMovieAd!");
    [self removeManAdView];
}

/* 동영상 광고 스킵
 */
- (void)didSkipMovieAd:(ManMovieAdView*)manMovieAd {
    NSLog(@"[ManMovieAdViewController] ================== didSkipMovieAd");
    [self startClientMovie];
}

/* 동영상 광고 클릭
 */
- (void)didClickMovieAd:(ManMovieAdView*)manMovieAd {
    NSLog(@"[ManMovieAdViewController] ================== didClickMovieAd");
        // * 참고 : 현재 시점에서 SPlus(MAN) 광고뷰가 제거 될 경우, didCloseRandingPage delegate함수를 호출 받을 수 없음.

    [self startClientMovie];
}

/* 동영상 광고 랜딩페이지 닫기버튼 클릭
 */
- (void)didCloseRandingPage:(ManMovieAdView*)manMovieAd {
    NSLog(@"[ManMovieAdViewController] ================== didCloseMovieAdRandingPage");
    [self startClientMovie];
}

/* 동영상 광고 없음
 */
- (void)didNotExistMovieAd:(ManMovieAdView*)manMovieAd {

    NSLog(@"[ManMovieAdViewController] ================== didNotExistMovieAd");
    [self startClientMovie];
}

#pragma mark MezzoMedia

/* 동영상 광고 URL 전달 - requestMovieAdURL호출에 대한 콜백
 */
- (void)responseMovieAdURL:(ManMovieAdView*)manMovieAd movieAdURL:(NSString*)movieAdURL {
    
    NSLog(@"[ManMovieAdViewController] ================== movieAdURL : %@", movieAdURL);
}



@end
